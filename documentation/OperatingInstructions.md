If you have successfully followed the detailed instructions for building and developing the SRAS payload, you will now have a working payload!

# The basic operating instructions are as follows: #
1. Power up the payload by connecting the USB Type A (big) connector to the battery and the USB micro (little) connector to the lower USB connection on the RaspBerry Pi Zero W
2. When SRAS is running, the LED that was added to the GPIO pins on the Pi will flash two long flashes; this means SRAS is collecting and saving data and pictures and can be connected to wirelessly using COSMOS (command and telemetry) or FileZilla (file transfer).  The LED may also flash short flashes (count the flashes to determine the digits, 10 flashes = the digit 0) indicating the IP address if it is within range of a wireless router that it can connect to.
3. Fly (or test or whatever) the payload
4. Shut the payload down in one of the following ways:
    - Short circuit together the pins on the header that was added to the GPIO pins on the Pi
    - Use the "SHUTDOWN" command from COSMOS
    - (Last resort is to pull the power plug; this is discouraged if possible as there is a possibility of corrupting the SD card)
5. Remove the SD card from the Pi
6. Plug the SD card into your team's computer and use Linux Reader to copy the files from the SD card to your computer
7. (Files can be found in subfolders of the folder /home/pi/<team name>-data, files include ".csv" or data, and ".jpg" or picture files)
8. Analyze the data in the ".csv" file.  This can be done in several ways, but one way is to use GNU Octave and the "viewer.m" file in the "data-analysis" folder.

# Additional instructions: #
- To communicate with the Pi payload wirelessly using either FileZilla or COSMOS, you will need to determine the IP (Internet Protocol) address of the Pi
    - The IP address is assigned automatically by the router, and is always 4 numbers separated by 3 periods; often it is something like "192.168.1.nnn", where the number "nnn" is assigned by the router
    - The LED will use short flashes to indicate the IP address.  Each digit is represented by the number of short flashes (10 flashes indicates the digit 0); pauses occur where the periods are in the IP address (between octets).  Remember, the long flashes just indicate the Pi operating system (first long flash) and data collection program (second long flash) are working.
- A program like "Fing" for the iPad/iPhone is useful for determining what IP addresses are active on your network; alternatively you can use "ping" to see what addresses are active (note that each number can only be between 0 and 255, numbers following 0 (1, 2, 3. ...) and following 100 (100, 101, 102, ...) are commonly assigned by routers)
- If you are using your payload in a class, make sure the IP address you find is yours!  When you connect with COSMOS or FileZilla, make sure you are looking at your team name (COSMOS ID telemetry packet, FileZilla look at the team name in /home/pi/<team name-data)
- For COSMOS, the IP address needs to be specified in the cmd_tlm_server.txt file
- For FileZilla, the IP address needs to be specified in the "Server" field

# Linux Reader instructions: #
Linux Reader is used for reading the files on the Pi SD card on the team computer.  To use Linux Reader:

1.  Start the Linux Reader program
2.  When the SD card is in the SD card reader, there will be a disk entry called "/rootfs".  Select it.
3.  Navigate on the "/rootfs" disk to "/home/pi/<team name>-data", as shown below.
4.  Select files to save to the team computer and choose to save them.

![Using the Linux Reader program](./LinuxReader.png)

For more information on Linux Reader, see [the DiskInternals Linux Reader website](https://www.diskinternals.com/linux-reader/).

# FileZilla instructions: #
FileZilla is used for copying files from the team computer to the Pi or from the Pi to the team computer.  To use FileZilla:

1. Start the FileZilla program
2. Connect to the Pi by:
    1. Fill in the IP address (e.g. "192.168.1.nnn") in the "Host" box
    2. Fill in "pi" in the "Username" box
    3. Fill in "sras" in the "Password" box
    4. Fill in "22" in the "Port" box
    5. Click on "Quickconnect"
3. After connecting, the team computer files are displayed on the left and the Pi files are displayed on the right
4. Drag and drop files from the left to the right to copy them from the team computer to the Pi
5. Drag and drop files from the right to the left to copy them from the Pi to the team computer

The picture below shows the user highlighting an image (.jpg) file on the Pi; if the user then drags the file to the left pane, it will be copied to the team computer.

![Using FileZilla to transfer data](./FileZillaData.png)

The picture below shows the user highlighting the "SRAS.py" Python file on the team computer; if the user then drags the file to the right pane, it will be copied to the Pi.  Note that the existing file will be overwritten.

![Using FileZilla to transfer the SRAS.py file](./FileZillaSRASpy.png)

For more information on FileZilla, see [the FileZilla project website](https://filezilla-project.org/).

# COSMOS instructions: #

On the team computer, use Windows Explorer to navigate to where the "Launcher.bat" file for the SRAS COSMOS installation was created (e.g. c:\Users\msuder\Documents\AfterHours\sras-cosmos)

Double clicking on "Launcher.bat" will result in the following window being shown.  This window can be used to start all of the other COSMOS tools.

![COSMOS Launcher](./COSMOSLauncher.png)

After the COSMOS launcher has been started, the first tool that is normally started is the Command and Telemetry Server.  This is selected by clicking on "Command and Telemetry Server" in the launcher.  After the user picks a system configuration file (the default is fine), the following window will be shown:

![COSMOS Command and Telemetry Server, connected interface](./COSMOSCnTServer1.png)

Note the button labeled "Disconnect" next to "SRAS_INT" in the window.  This button is used to connect and disconnect from the SRAS Pi payload.  In this case, note that the user has already connected, as indicated by the green "True" under the "Connected?" column.  In addition the number of telemetry packets received ("Tlm Pkts") and number of bytes received ("Bytex Rx") by COSMOS from the SRAS Pi payload is shown.  In addition, the number of commands sent ("Cmd Pkts") and the number of bytes sent ("Bytex Tx") by COSMOS to the SRAS Pi payload is shown. 

The following window shows another tab in the Command and Telemetry server window.  This tab shows the number of telemetry packets of each type that have been received (e.g. SRAS ENV) and has a button to launch the packet viewer to view the latest packet received.  Note that the window below can also be displayed by clicking on "Packet Viewer" on the launcher. 

![COSMOS Command and Telemetry Server, packet statistics](./COSMOSCnTServer2.png)

The following window shows the COSMOS Command Sender.  This window can be displayed by clicking on "Command Sender" in the COSMOS launcher.  In this case, the user has chosen the "REBOOT" command to send.  This command has been marked as hazardous (because you are rebooting the payload!) in the SRAS COSMOS command database, so the user is being prompted with a confirmation dialog before proceeding with sending the command.

![COSMOS Command Sender](./COSMOSCommandSender.png)

The following window shows the COSMOS Packet Viewer.  This window can be displayed by clicking on "Packet Viewer" in the COSMOS launcher.  This window shows only the **latest** data from the payload for this packet.  In this case the SRAS CONFIG packet data is being shown.  This packet displays some crucial data about the payload including:  time on the Pi of the packet ("DATETM"), number of lines of data written to the CSV data file on the Pi ("NUMLINS"), number of pictures taken by the Pi ("NUMPICS"), the size of the Pi SD card in MB ("FSIZE"), and the size of the free space on the Pi SD card in MB ("FAVAIL").  The last two can be used to determine if the Pi SD card is running out of space (and needs some of the SRAS data/pictures deleted)! 

![COSMOS Packet Viewer](./COSMOSPacketViewer.png)

This final window shows the COSMOS Telemetry Grapher.  This window can be displayed by clicking on "Telemetry Grapher" in the COSMOS launcher.  To get the display below, the user must then choose "Load configuration" and choose the "SRAS.txt" configuration file.  This graph has tabs to show the changing summary IMU, environmental, accelerometer, gyroscope, and magnetometer data as it is received from SRAS.

![COSMOS Telemetry Grapher](./COSMOSTelemetryGrapher.png)

For more information on COSMOS, see [the Ball Aerospace COSMOS website](http://cosmosrb.com/).

# GNU Octave instructions: #

1. Start the GNU Octave (GUI) program
2. Use "File -> Open..." to open the "viewer.m" file.  This file can be found and saved from the /sras/data-analysis directory
3. Click on the "Save File and Run" icon, which looks like a yellow triangle inside a grey gear
4. Click "Change Directory" if prompted with a dialog box to "Change Directory or Add Directory to Load Path"
5. Select the rocket telemetry ".csv" file that you wish to analyze when prompted
6. Wait while "Loading Time Series..."

![Loading Time Series](./LoadingTimeSeries.png)

7. Click the mouse two times in the resulting "Select Time Axis Limits" figure.  The x position of each of the clicks will be used to determine a time on the x axis.  These two times will define the time span to be analyzed.

![Select Time Axis Limits](./SelectTimeAxisLimits.png)

8. Wait while "Calculating Trajectory..."

![Calculating Trajectory](./CalculatingTrajectory.png)

8. Figures showing graphs of position, acceleration, magnetic field, temperature, and pressure (all vs. time) will be displayed.

![Position Graph](./PositionGraph.png)


For more information on GNU Octave, see [the GNU Octave website](https://www.gnu.org/software/octave/).
