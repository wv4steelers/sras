### Build a TARC Rocket ###
The payload bay must be a BT-80 (65 mm inside diameter) tube with at least 165 mm interior room and 175 g payload capability for the SRAS payload.

This step is not covered, but you can refer [here](http://rocketcontest.org) and [here](http://3384f12ld0l0tjlik1fcm68s.wpengine.netdna-cdn.com/wp-content/uploads/2015/07/TARC-1-REV-18-2018-Rocket-Contest-Rules-final.pdf) for more information on TARC.

### 3-D print the sled ###
This step is also not covered, but the STL files for the sled design are in the 3DSledParts folder.  There are also files for MakerBot model printers/software.

### Solder wires from the Pi Zero W to the IMU ###

If you are unfamiliar with soldering, here is a short video discussing how soldering is done:  [https://youtu.be/f95i88OSWB4](https://youtu.be/f95i88OSWB4)

Connections:

1. Pi pin 1 to IMU pin 3.3V (red wire, 3.3V)
2. Pi pin 3 to IMU pin SDA (white wire, I2C SDA)
3. Pi pin 5 to IMU pin SCL (green wire, I2C SCL)
4. Pi pin 9 (make sure to SKIP pin 7) to IMU pin GND (black wire, ground)

![SRAS Wiring Image](./SRAS_Wiring.png)

![Pi Zero W Pinout](./pi_zero_w_pinout.jpg)

### Solder the LED/resistor and Header Pins ###
1. Pi pin 16 (GPIO 23) to 330 Ohm resistor, 330 Ohm resistor to LED anode (+), LED cathode (-) to Pi pin 30 (GND)

*This LED provides payload status and the IP address of the Pi*

*The LED blinks one long blink when it has started up and two long blinks if the payload sensor program is running*

*After the long blinks, the LED blinks out the digits of the IP address, with pauses between each octet (zero is represented by 10 blinks)*

2. One header pin in Pi pin 32 (GPIO 12) and one header pin in Pi pin 34 (GND)

*These pins are shorted/jumped in order to send a signal to the sensor program to shut the Pi down.*

### Assemble the sled and electronics ###
**Electronics panel** - Use 6 nylon screws and nuts to attach the IMU and the Pi to the panel (may need to slightly enlarge the Pi mounting holes with a 1/8" drill bit if 3mm bolts are used).

**Battery panel** - Attach the battery with two twist ties, make sure the body of the battery is toward the center of the sled.

**Camera** - Mount the camera on the base pins; use double sided tape if desired for a more secure mount.  Attach the ribbon cable to the camera and thread it through the slot in the battery panel and attach the other end to the Pi Zero W camera connector.

Slide the panels into the base and slide the top onto the panels.

Thread cord through the holes in the top and base of the sled to make a loop around the entire sled.  Tie a knot.  The cord will help hold the 4 pieces together and will provide a way to pull the SRAS payload out of the TARC rocket.

### Burn the SD Card for the Pi Zero W ###
1. Insert the Micro SD card in the SD card adapter and insert it into an SD card reader in your laptop.
2. Download the file "/sras/PiBakery/recipe.xml" from this website to your team computer (click "<> Source", then navigate to PiBakery, then click "recipe.xml", then click "Raw", then right click on the raw data and choose "Save As" to save the file.
3. Start the PiBakery program.
4. Choose "Import" and select the "recipe.xml" file that you downloaded.
5. The yellow circled fields need customized for your team/class:
    1. Set hostname to:  This should be filled in with your team name.  This name will be used in COSMOS telemetry and in the data folder/file names to identify your team's SRAS payload data and pictures.
	2. Network:  Change this to the name of the WiFi router specified in your class.  This should be a router with internet access so the Pi can download needed software.
    3. Pass:  Fill in the password for the WiFi router specified in your class.
	4. Note that for the class we held, we took a router (WVRock) to the launch site/field, so there is a second WiFi network listed that has no password.  You can do the same if you like or not use a second WiFi network.
6. Click "Write"
7. Choose the correct drive letter for your SD card.  **MAKE SURE YOU PICK YOUR SD CARD DRIVE LETTER.  PICKING YOUR OPERATING SYSTEM DRIVE LETTER COULD DESTROY YOUR TEAM COMPUTER.**
8. Choose "Raspbian Full" for the operating system.
9. Click "Start Write" and watch the progress bar as the SD card is written.
10. "Writing to SD" and a progress bar will be displayed... the writing will take 5 to 10 minutes to complete.
10. When this is done,  "Write Successful" will be displayed.  Click "Close".
11. Eject the SD card from the SD card reader in your laptop and remove the Micro SD card from the SD card adapter

![PiBakery program](./PiBakery.png)

### Insert the SD Card in the Pi Zero W and Start It Up ###
1. Eject the SD card from the SD card reader on the team computer
2. Plug the SD card into the Raspberry Pi Zero W
3. Power on the Raspberry Pi Zero W by connecting the USB A (big) connector to the battery and the USB micro (little) connector to the lower USB connector on the Pi

This step might take 5 to 10 minutes or so, during which time the Pi will reboot several times.  The tiny green LED on the Pi will be lit when the Pi is on.  When the SRAS program is finally loaded and starts, the big LED that was soldered to the Pi GPIO pins will be blinking.

After the initial time of installing/configuring the SRAS Pi, it should take less than a minute from power on until the SRAS LED blinks.

*The LED blinks one long blink when it has started up and two long blinks if the payload sensor program is running*

*After the long blinks, the LED blinks out the digits of the IP address, with pauses between each octet (zero is represented by 10 blinks)*

Note:  Occasionally, I have had trouble with the PiBakery program not completely setting up the SD card/Raspbian with all of the settings, packages, etc. that are needed.  In this case, you can use FileZilla (see [the operating instructions](./OperatingInstructions.md)):  using FileZilla, create (empty) files on the Pi in "/boot/PiBakery" called "runFirstBoot" and "runNextBoot".  These files will cause PiBakery to rerun the setup steps again when the Pi is rebooted.

### Key Physical Concepts ###
1. Temperature
2. Air pressure
3. 3 dimensions and x/y/z coordinates
4. Acceleration/force
5. Gyroscope/angular rate
6. Magnetic field

### Key Programming Concepts ###
1. Python language
2. spaces matter
3. functions
4. variables
5. arrays
6. format specifiers
7. if statement

### Program the SD Card in Python ###
1. Use FileZilla (see [the operating instructions](./OperatingInstructions.md)) to download the file "/home/pi/sras/PiDataCollectionScripts/SRAS.py" from the Pi to your team computer.

2. You will probably also want to download "/home/pi/sras/PiDataCollectionScripts/SensorCollector.py" since it describes many useful functions we will use in SRAS.py.

3. Edit the file "SRAS.py" (using Notepad++ or another text file editor of your choice)
    1. You should ONLY add lines after "# YOUR SETUP CODE HERE" or "# YOUR LOOP CODE HERE"
	2. Anything you put in "setup" will get executed once, when the payload starts.
	3. Anything you put in "loop" will get executed 50 times every second (approximately).
	4. We are programming SRAS.py in Python and will talk about the lines we are adding to "setup" and "loop" in SRAS.py, but note that they should end up looking just like "setup" (lines 112-114) and "loop" (lines 116-136) in SensorCollector.py.
    5. The functions that are named beginning with "def" on lines 23-111 in "SensorCollector.py" are the functions we will use to program "setup" and "loop" in SRAS.py.  The current list of functions is:
        1. getdate
		2. getname
		3. getenvdata
		4. getaccel
		5. getgyro
		6. getmag
		7. opendatafile
		8. writedatafile
		9. takepicture
	6. In "setup", we are going to need to call "opendatafile" to make sure the data file is ready for us to write data to it
	7. In "loop", we are going to get the environment, accelerometer, gyroscope, and magnetometer data
	8. In "loop", we will then write the team name, date/time, and environment, accelerometer, gyroscope, and magnetometer data to the data file.  Note that this will happen 50 times per second.
    9. In "loop", we also want to take a picture, but only every 50 times through the loop (i.e. only every second)

    10. (An alternative shortcut where you won't learn as much coding is to comment out (put a # in front of) the lines that say "def setup", "pass", "def loop", and "pass" in SRAS.py.  This will cause the versions of the functions "setup" and "loop" in SensorCollector.py to be used instead of the ones in SRAS.py.)

4. Use FileZilla (see [the operating instructions](./OperatingInstructions.md)) to upload the modified file "SRAS.py" to the Pi at "/home/pi/sras/PiDataCollectionScripts/SRAS.py" from your team computer to the Pi.

### Configure and use the Ball Aerospace COSMOS ground station ###
See "COSMOS instructions" in [the operating instructions](./OperatingInstructions.md).

### Launch and retrieve the TARC rocket with the SRAS payload ###
This step is not covered, but refer [here](http://www.nar.org/safety-information/model-rocket-safety-code/) for some important safety rules.

Make sure you power on the payload prior to flight and that the LED periodically has two long flashes, indicating that the data collection program is running (only one long flash indicate the Pi has completely started up, but the data collection program is not running).

### Read the IMU data and the camera pictures from the SD card ###
See "Linux Reader instructions" in [the operating instructions](./OperatingInstructions.md).

### Analyze the IMU data ###
See "GNU Octave instructions" in [the operating instructions](./OperatingInstructions.md).
