### 3-D printed sled: ###
- Base
- Top
- Battery Panel
- Electronics Panel

(see the design files in the 3DSledParts subdirectory for these parts)

### Computer Programs: ###
- PiBakery [http://www.pibakery.org](http://www.pibakery.org) - Software used to initialize the SD card with the Raspbian operating system and the SRAS software that is used by the payload
- FileZilla Client [https://filezilla-project.org/](https://filezilla-project.org/) - Software used to copy files over the wireless internet between the payload and the team computer
- Notepad++ [https://notepad-plus-plus.org/](https://notepad-plus-plus.org/) - Text editor for creating and modify SRAS.py Python program (other editors can be used, but I like this one)
- Linux Reader [https://www.diskinternals.com/linux-reader/](https://www.diskinternals.com/linux-reader/) - Software for reading data files and pictures from the SD card after flights
- COSMOS [http://cosmosrb.com/](http://cosmosrb.com/) - Optional, BUT... if you want to monitor your payload wirelessly, this is really cool (and we use it for [STF-1](http://stf1.com/), West Virginia's first spacecraft)!
- GNU Octave [https://www.gnu.org/software/octave/](https://www.gnu.org/software/octave/) - Also optional... but can be used to create graphs of the data from the run.

### Electronics: ###
- Pi Zero W - Raspberry Pi Zero W [https://www.amazon.com/gp/product/B0748MBFTS/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B0748MBFTS/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
- IMU - BerryIMUv2 - 10 DOF [https://www.amazon.com/gp/product/B072MN8ZRC/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B072MN8ZRC/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
- Camera - Makerfocus Raspberry Pi Camera with adapter cable for Pi Zero W [https://www.amazon.com/gp/product/B075149PWB/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B075149PWB/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)
- Battery - Anker PowerCore+ mini, 3350mAh Lipstick-Sized Portable Charger (the sled is designed for the shape of this battery) [https://www.amazon.com/gp/product/B005X1Y7I2/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B005X1Y7I2/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)
- SD Card - Micro SD Card with adapter [https://www.amazon.com/gp/product/B010Q57SEE/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B010Q57SEE/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
- 90 Degree cable [https://www.amazon.com/SUNGUY-Charging-Samsung-Galaxy-Motorola/dp/B07C1HRKCJ/ref=sr_1_6?ie=UTF8&qid=1530118742&sr=8-6&keywords=usb+charger+micro+90+degree](https://www.amazon.com/SUNGUY-Charging-Samsung-Galaxy-Motorola/dp/B07C1HRKCJ/ref=sr_1_6?ie=UTF8&qid=1530118742&sr=8-6&keywords=usb+charger+micro+90+degree)

### Miscellaneous: ###
- 4 - wires 70 mm long (preferably different colors; preferably red (3.3V), white (I2C SDA), green (I2C SCL), black (ground)
- 1 - LED
- 1 - 330 Ohm (approximately, does not have to be exact) resistor
- 1 - 2 pin header
- 6 - nylon bolts 2.5 or 3 mm diameter and 5 mm long with 6 nuts
- A piece of cord about 40 cm long (hold sled together, pull sled out of rocket)
- 2 - twist ties (battery holders)
- A couple small pieces of double sided tape (hold camera in place)
- (OPTIONAL) - If you look at some of my pics, I have a 100 uF electrolytic capacitor between pins 2 (5V) and 6 (GND) to smooth out any power glitches.  I'm not sure how successful or necessary this is, so it is just optional.

### Tools: ###
- Soldering iron/station
- Fine screw drivers
- Wire strippers / diagonal cutters
- Clamps, tweezers, piece holders, etc.