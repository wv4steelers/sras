2018-11-11
Third launch
Liftoff approx 2018-11-11, 16:15:18.6 laptop time = 2018-11-01, 01:41:11.7 Pi time

Firefly altimeter data:
Apogee = 525 ft
Speed = 261 ft/sec
Time to Apogee = 5.0 seconds
Flight Time = 32.8 seconds
Descent Rate = 18.8 ft/sec

Motor: 50-F51-BS-13A-6, Cessaroni 2 grain 24mm, F51 Blue Streak, set for 6 second delay
Rocket:  See SRAS.ork rocket in rockets folder

Weather:  Sunny, clear skies, no wind, temperature in mid 40s
