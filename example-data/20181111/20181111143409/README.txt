2018-11-11
First launch
Liftoff approx 2018-11-11, 14:34:09.7 laptop time = 2018-11-01, 01:15:05 Pi time
Data cut out at liftoff

Firefly altimeter data:
Apogee = 532 ft
Speed = 253 ft/sec
Time to Apogee = 5.2 seconds
Flight Time = 34.0 seconds
Descent Rate = 18.4 ft/sec

Motor: 50-F51-BS-13A-6, Cessaroni 2 grain 24mm, F51 Blue Streak, set for 6 second delay
Rocket:  See SRAS.ork rocket in rockets folder

Weather:  Sunny, clear skies, no wind, temperature in mid 40s
