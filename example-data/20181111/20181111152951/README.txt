2018-11-11
Second launch
Liftoff approx 2018-11-11, 15:29:51.1 laptop time = 2018-11-01, 01:16:39.7 Pi time

Firefly altimeter data:
Apogee = 559 ft
Speed = 258 ft/sec
Time to Apogee = 5.2 seconds
Flight Time = 35.9 seconds
Descent Rate = 18.2 ft/sec

Motor: 50-F51-BS-13A-6, Cessaroni 2 grain 24mm, F51 Blue Streak, set for 6 second delay
Rocket:  See SRAS.ork rocket in rockets folder

Weather:  Sunny, clear skies, no wind, temperature in mid 40s
