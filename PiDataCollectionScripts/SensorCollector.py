#!/usr/bin/python

import os
import shutil
import pwd
import signal
import sys
import time
import threading
import socket
import datetime
import binascii
import struct
import ConfigParser
import RPi.GPIO as GPIO
import bmp280
import IMU
import picamera

# Class to collect data from sensors/camera, etc.
class SensorCollector(object):

### Methods/API intended to be used by inheritors of this class ###
  # Returns a formatted string with the date/time
  def getdate(self):
    return str(datetime.datetime.now())

  # Returns the name (team name) of the computer/payload
  def getname(self):
    return os.uname()[1] 

  # Returns environmental data: temperature (Celsius), 
  # temperature (Fahrenheit), air pressure (millibars)
  def getenvdata(self):
    if (self._bmp is not None):
      d = self._bmp.getdata() # (tempC, tempF, press)
      return ((d[0]+self._temp_c_offset)*self._temp_c_scale, \
              (d[1]+self._temp_f_offset)*self._temp_f_scale, \
              (d[2]+self._press_mb_offset)*self._press_mb_scale)
    else:
      return self._badvalues

  # NOTE:  Conversions below rotate from IMU axes to rocket payload axes, 
  # with +z = up, +x = out through camera, +y = right (facing camera)

  # Sensor characteristic multipliers from:  
  #  http://ozzmaker.com/wp-content/uploads/2017/11/LSM9DS1.pdf, 
  #  Section 2.1, Table 3 0.58
  # Also note that the sensor axes seem goofy to me... accel/gyro left handed,
  # mag right handed, axes of the two don't line up (+z does)

  # +/- 16 g, multiply by 0.001 * 0.732 
  # Verify using the fact that up should be 1g

  # Returns acceleration in g's: x, y, and z directions
  def getaccel(self):
    if (self._imuok):
      d = (0.001*0.732*IMU.readACCz(), 0.001*0.732*IMU.readACCy(), 0.001*0.732*IMU.readACCx())
      return ((d[0]+self._acc_x_offset)*self._acc_x_scale, \
              (d[1]+self._acc_y_offset)*self._acc_y_scale, \
              (d[2]+self._acc_z_offset)*self._acc_z_scale)
    else:
      return self._badvalues

  # +/- 2000 deg/sec, multiply by 0.001 * 70
  # Verify using 33 1/3 RPM turntable = 200 degrees per second

  # Returns gyroscope rates in degress/second: x, y, and z directions
  def getgyro(self):
    if (self._imuok):
      d = (0.001*70*IMU.readGYRz(), 0.001*70*IMU.readGYRy(), 0.001*70*IMU.readGYRx())
      return ((d[0]+self._gyro_x_offset)*self._gyro_x_scale, \
              (d[1]+self._gyro_y_offset)*self._gyro_y_scale, \
              (d[2]+self._gyro_z_offset)*self._gyro_z_scale)
    else:
      return self._badvalues

  # +/- 12 Gauss, multiply by 0.001 * 0.43
  # Verify using Magnetic Field Calculator at https://www.ngdc.noaa.gov/geomag-web/?useFullSite=true#igrfwmm

  # Returns magnetic field reading in Gauss: x, y, and z directions
  def getmag(self):
    if (self._imuok):
      d = (0.001*0.43*IMU.readMAGz(), 0.001*0.43*IMU.readMAGy(), -0.001*0.43*IMU.readMAGx())
      return ((d[0]+self._mag_x_offset)*self._mag_x_scale, \
              (d[1]+self._mag_y_offset)*self._mag_y_scale, \
              (d[2]+self._mag_z_offset)*self._mag_z_scale)
    else:
      return self._badvalues

  # Opens the data file for writing... must be called in "setup" before
  # calling "writedatafile"
  def opendatafile(self):
    now = datetime.datetime.now()
    pi = pwd.getpwnam("pi")
    self._datafilename = self._directory + "/%s-%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d.csv" % (os.uname()[1], now.year, now.month, now.day, now.hour, now.minute, now.second)
    os.mknod(self._datafilename, 0644)
    os.chown(self._datafilename, pi[2], pi[3])
    self._datafile = open(self._datafilename, "w")

  # Writes a string of data to the datafile.  Note that the data file must
  # have been opened with "opendatafile" prior to calling this method
  def writedatafile(self, string):
    self._datafile.write(string)
    self._datalines += 1

  # Causes the program to take a picture with the camera
  def takepicture(self):
    self._take_picture = True

### Methods used by this class itself, overriden in SRAS.py ###
  # This is what SRAS.py setup should look like
  def setup(self):
    self.opendatafile()

  # Sensor/camera processing every time around the loop
  # This is what SRAS.py loop should look like
  def loop(self):
    envdata = self.getenvdata()
    accel = self.getaccel()
    gyro = self.getgyro()
    mag = self.getmag()

    string = "%s,%s, %f,%f,%f, %f,%f,%f, %f,%f,%f, %f,%f,%f\n" % \
      (self.getname(), self.getdate(), \
       envdata[0], envdata[1], envdata[2], \
       accel[0], accel[1], accel[2], \
       gyro[0], gyro[1], gyro[2], \
       mag[0], mag[1], mag[2])
    self.writedatafile(string)
 
    self._count += 1
    if (self._count >= 50):
      self.takepicture()
      self._count = 0
 
  # Member data
  _count = 0

### Methods used by this class itself ###
  def __init__(self):
    # Install signal handlers
    signal.signal(signal.SIGINT, self.signal_handler)
    signal.signal(signal.SIGTERM, self.signal_handler)

    # Set up directories for this run and log files
    pi = pwd.getpwnam("pi")
    try:
      os.mkdir(self._directory, 0755)
    except Exception as e:
      print(e)
    os.chown(self._directory, pi[2], pi[3])
    now = datetime.datetime.now()
    self._directory += "/%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d" % (now.year, now.month, now.day, now.hour, now.minute, now.second)
    while (os.path.isdir(self._directory)):
      self._directory += "-" # Keep appending - until the name is unique
    os.mkdir(self._directory, 0755)
    os.chown(self._directory, pi[2], pi[3])

    sys.stdout = open(self._directory + "/log.txt", "wb", 0)
    os.chown(self._directory + "/log.txt", pi[2], pi[3])
    sys.stderr = open(self._directory + "/err.txt", "wb", 0)
    os.chown(self._directory + "/err.txt", pi[2], pi[3])
    self.read_config()

    # Set up instruments
    try:
      self._bmp = bmp280.BMP280()
    except Exception as e:
      print(e)
    try:
      IMU.detectIMU()
      IMU.initIMU()
      self._imuok = True
    except Exception as e:
      print(e)
    try:
      self._camera = picamera.PiCamera()
      self._camera.resolution = (1024, 768)
      self._camera.rotation = 180
      #self._camera.shutter_speed = 4000 # us = 1/250 s
      self._camera.exposure_mode = 'sports' # Constraints min shutter spd 1/60
      self._camera.annotate_text_size = 24
    except Exception as e:
      self._camera = None
      print(e)
    time.sleep(2) # Camera warm-up time

    # GPIO setup 
    GPIO.setmode(GPIO.BCM)

    # Get my IP address
    self._my_ip = ((([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0])

    # Threads
    self._cosmos_cmd_thread = threading.Thread(target=self.cosmos_cmd_handler, args=())
    self._cosmos_cmd_thread.start()
    self._cosmos_tlm_thread = threading.Thread(target=self.cosmos_tlm_handler, args=())
    self._cosmos_tlm_thread.start()
    self._picture_thread = threading.Thread(target=self.picture_handler, args=())
    self._picture_thread.start()

    # Call user setup function
    self.setup()
 
  def signal_handler(self, signal, frame):
    print("Caught SIGINT... terminating program.")
    self._not_done = False

  # Function executed in a separate thread to receive COSMOS CMD over TCP.
  def cosmos_cmd_handler(self):
    if (self._cosmos_protocol == "tcp"):
      cmd_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    elif (self._cosmos_protocol == "udp"):
      cmd_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    else:
      print("Invalid COSMOS protocol %s for command handler.  Ending command handler" % 
              self._cosmos_protocol)
      return
    cmd_sock.bind((self._any_ip, self._cosmos_tcp_cmd_port))
    print("Command socket protocol %s bound to IP %s, address %d" % 
            (self._cosmos_protocol, self._any_ip, self._cosmos_tcp_cmd_port))
    if (self._cosmos_protocol == "tcp"):
      cmd_sock.listen(5)
    cmd_sock.settimeout(self._loop_rate)

    cmd_conn = None
    while self._not_done:
      try:
        if (self._cosmos_protocol == "tcp"):
          cmd_conn, addr = cmd_sock.accept()
        while True:
          if (self._cosmos_protocol == "tcp"):
            command = cmd_conn.recv(4096)
          else:
            command, addr = cmd_sock.recvfrom(1024) # buffer size is 1024 bytes
            print("Sending to %s" % addr[0])
            self._cosmos_ip[addr[0]] = 1
          if (not command):
            break
          self._cos_cmd_recv += 1
          if (command[1] == 'e'): # 101, HELP
            print(self._command_list)
            self._cos_cmd_succ += 1
          if (command[1] == 'f'): # 102, QUIT
            self._not_done = False
            self._cos_cmd_succ += 1
          if (command[1] == 'g'): # 103, SHUTDOWN
            self._not_done = False
            self._shut_down = True
            self._cos_cmd_succ += 1
          if (command[1] == 'h'): # 104, REBOOT
            self._not_done = False
            self._reboot = True
            self._cos_cmd_succ += 1
          if (command[1] == 'i'): # 105, DEBUG
            self._debug = True
            self._cos_cmd_succ += 1
          if (command[1] == 'j'): # 106, LOOPDEBUG
            self._loop_debug = True
            self._cos_cmd_succ += 1
          if (command[1] == 'k'): # 107, NODEBUG
            self._debug = False
            self._loop_debug = False
            self._cos_cmd_succ += 1
          if (command[1] == 'n'): # 110, DELETEOLD
            self.deleteolddata()
            self._cos_cmd_succ += 1
          if (command[1] == 'o'): # 111, NOOP
            self._cos_cmd_succ += 1
          if (command[1] == 'p'): # 112, ACC CONFIG
            self._acc_x_offset = struct.unpack("f",
              bytearray((command[9], command[8], command[7], command[6])))[0]
            self._acc_x_scale = struct.unpack("f",
              bytearray((command[13], command[12], command[11], command[10])))[0]
            self._acc_y_offset = struct.unpack("f",
              bytearray((command[17], command[16], command[15], command[14])))[0]
            self._acc_y_scale = struct.unpack("f",
              bytearray((command[21], command[20], command[19], command[18])))[0]
            self._acc_z_offset = struct.unpack("f",
              bytearray((command[25], command[24], command[23], command[22])))[0]
            self._acc_z_scale = struct.unpack("f",
              bytearray((command[29], command[28], command[27], command[26])))[0]
            self.write_config()
            self._cos_cmd_succ += 1
          if (command[1] == 'q'): # 113, GYRO CONFIG
            self._gyro_x_offset = struct.unpack("f",
              bytearray((command[9], command[8], command[7], command[6])))[0]
            self._gyro_x_scale = struct.unpack("f",
              bytearray((command[13], command[12], command[11], command[10])))[0]
            self._gyro_y_offset = struct.unpack("f",
              bytearray((command[17], command[16], command[15], command[14])))[0]
            self._gyro_y_scale = struct.unpack("f",
              bytearray((command[21], command[20], command[19], command[18])))[0]
            self._gyro_z_offset = struct.unpack("f",
              bytearray((command[25], command[24], command[23], command[22])))[0]
            self._gyro_z_scale = struct.unpack("f",
              bytearray((command[29], command[28], command[27], command[26])))[0]
            self.write_config()
            self._cos_cmd_succ += 1
          if (command[1] == 'r'): # 114, MAG CONFIG
            self._mag_x_offset = struct.unpack("f",
              bytearray((command[9], command[8], command[7], command[6])))[0]
            self._mag_x_scale = struct.unpack("f",
              bytearray((command[13], command[12], command[11], command[10])))[0]
            self._mag_y_offset = struct.unpack("f",
              bytearray((command[17], command[16], command[15], command[14])))[0]
            self._mag_y_scale = struct.unpack("f",
              bytearray((command[21], command[20], command[19], command[18])))[0]
            self._mag_z_offset = struct.unpack("f",
              bytearray((command[25], command[24], command[23], command[22])))[0]
            self._mag_z_scale = struct.unpack("f",
              bytearray((command[29], command[28], command[27], command[26])))[0]
            self.write_config()
            self._cos_cmd_succ += 1
          if (command[1] == 's'): # 115, ENV CONFIG
            self._temp_c_offset = struct.unpack("f",
              bytearray((command[9], command[8], command[7], command[6])))[0]
            self._temp_c_scale = struct.unpack("f",
              bytearray((command[13], command[12], command[11], command[10])))[0]
            self._temp_f_offset = struct.unpack("f",
              bytearray((command[17], command[16], command[15], command[14])))[0]
            self._temp_f_scale = struct.unpack("f",
              bytearray((command[21], command[20], command[19], command[18])))[0]
            self._press_mb_offset = struct.unpack("f",
              bytearray((command[25], command[24], command[23], command[22])))[0]
            self._press_mb_scale = struct.unpack("f",
              bytearray((command[29], command[28], command[27], command[26])))[0]
            self.write_config()
            self._cos_cmd_succ += 1
          if (command[1] == 't'): # 116, STOP SENDING
            print("stop sending command")
            # remove key from self._cosmos_ip
            if (self._cosmos_protocol != "tcp"):
              del self._cosmos_ip[addr[0]]
              print("removing %s" % addr[0])
            self._cos_cmd_succ += 1
      except socket.timeout:
        pass # This will happen often... do nothing
      except Exception as e:
        print(e)
      finally:
        if (cmd_conn is not None):
          cmd_conn.close()

  def deleteolddata(self):
    contents = os.listdir(self._basedirectory)
    for sub in contents:
      if (self._basedirectory + "/" + sub == self._directory):
        pass # Do not delete current directory we are writing to
      else:
        shutil.rmtree(self._basedirectory + "/" + sub)

  # Function executed in a separate thread to send COSMOS TLM over TCP.
  def cosmos_tlm_handler(self):
    if (self._cosmos_protocol == "tcp"):
      tlm_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    elif (self._cosmos_protocol == "udp"):
      tlm_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    else:
      print("Invalid COSMOS protocol %s for command handler.  Ending command handler" % 
              self._cosmos_protocol)
      return
    if (self._cosmos_protocol == "tcp"):
      tlm_sock.bind((self._any_ip, self._cosmos_tcp_tlm_port))
      tlm_sock.listen(5)
    tlm_sock.settimeout(self._loop_rate)

    tlm_conn = None
    while self._not_done:
      try:
        if (self._cosmos_protocol == "tcp"):
          tlm_conn, addr = tlm_sock.accept()
        lastenv = datetime.datetime.now()
        lastimu = lastconfig = lastid = lastenv
        while self._not_done:
          now = datetime.datetime.now()
          if (now > lastenv + datetime.timedelta(0, self._cosmos_envtlm_delay)):
            lastenv = now
            data = self.create_env_packet()
            if (self._cosmos_protocol == "tcp"):
              tlm_conn.sendall(data)
            else:
              for ip in self._cosmos_ip:
                tlm_sock.sendto(data, (ip, self._cosmos_tcp_tlm_port))
          if (now > lastimu + datetime.timedelta(0, self._cosmos_imutlm_delay)):
            lastimu = now
            data = self.create_imu_packet()
            if (self._cosmos_protocol == "tcp"):
              tlm_conn.sendall(data)
            else:
              for ip in self._cosmos_ip:
                tlm_sock.sendto(data, (ip, self._cosmos_tcp_tlm_port))
          if (now > lastconfig + datetime.timedelta(0, self._cosmos_configtlm_delay)):
            lastconfig = now
            data = self.create_config_packet()
            if (self._cosmos_protocol == "tcp"):
              tlm_conn.sendall(data)
            else:
              for ip in self._cosmos_ip:
                tlm_sock.sendto(data, (ip, self._cosmos_tcp_tlm_port))
          if (now > lastid + datetime.timedelta(0, self._cosmos_idtlm_delay)):
            lastid = now
            data = self.create_id_packet()
            if (self._cosmos_protocol == "tcp"):
              tlm_conn.sendall(data)
            else:
              for ip in self._cosmos_ip:
                tlm_sock.sendto(data, (ip, self._cosmos_tcp_tlm_port))
          time.sleep(self._loop_rate)
      except socket.timeout:
        pass # This will happen often... do nothing
      except Exception as e:
        print(e)
      finally:
        if (tlm_conn is not None):
          tlm_conn.close()

  def picture_handler(self):
    while self._not_done:
      try:
        if (self._take_picture and (self._camera is not None)):
          now = datetime.datetime.now()
          pi = pwd.getpwnam("pi")
          picturefile = "%s-%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d" % \
                  (os.uname()[1], now.year, now.month, now.day, \
                   now.hour, now.minute, now.second)
          while (os.path.exists(self._directory + "/" + picturefile + ".jpg")):
            picturefile += "-" # Keep appending - to make it unique
          picturepath = self._directory + "/" + picturefile + ".jpg"
          os.mknod(picturepath, 0644)
          os.chown(picturepath, pi[2], pi[3])

          self._camera.annotate_text = \
                  "%s-%4.4d-%2.2d-%2.2dT%2.2d:%2.2d:%2.2d.%6.6d" % \
                  (os.uname()[1], now.year, now.month, now.day, \
                   now.hour, now.minute, now.second, now.microsecond)
          self._camera.capture(picturepath)
          self._number_of_pics += 1

          self._take_picture = False
      except Exception as e:
        print(e)
      time.sleep(self._loop_rate)

  def create_env_packet(self):
    # tempC, tempF, pressure
    # preamble
    if (self._cosmos_protocol == "tcp"):
      data = bytearray((0xDE, 0xAD, 0x4E, 0xAD))
    else:
      data = bytearray()
    # CCSDS primary header
    data.extend((0x0C, 0x01))
    # sequence
    data.extend((0xC0 | ((self._env_sequence >> 8) & 0xFF), self._env_sequence & 0xFF))
    self._env_sequence = self._env_sequence + 1
    if (self._env_sequence >=16384):
        self._env_sequence = 0
    # length - 1
    data.extend((0x00,0x21))
    # CCSDS secondary header; 4 byte time
    # CCSDS secondary header, ASCII segmented time code
    now = datetime.datetime.now()
    data.extend("%4.4d-%2.2d-%2.2dT%2.2d:%2.2d:%2.2d.%2.2d" % (now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond/10000))
    # Environment data
    (tempC, tempF, press) = self.getenvdata()
    ba = bytearray(struct.pack("f", tempC))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", tempF))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", press))
    data.extend(reversed(ba))

    return data 

  def create_imu_packet(self):
    # accel, gyro, mag
    # preamble
    if (self._cosmos_protocol == "tcp"):
      data = bytearray((0xDE, 0xAD, 0x4E, 0xAD))
    else:
      data = bytearray()
    # CCSDS primary header
    data.extend((0x0C, 0x02))
    # sequence
    data.extend((0xC0 | ((self._imu_sequence >> 8) & 0xFF), self._imu_sequence & 0xFF))
    self._imu_sequence = self._imu_sequence + 1
    if (self._imu_sequence >=16384):
        self._imu_sequence = 0
    # length - 1
    data.extend((0x00,0x39))
    # CCSDS secondary header; 4 byte time
    # CCSDS secondary header, ASCII segmented time code
    now = datetime.datetime.now()
    data.extend("%4.4d-%2.2d-%2.2dT%2.2d:%2.2d:%2.2d.%2.2d" % (now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond/10000))
    # IMU data
    (accx, accy, accz) = self.getaccel()
    ba = bytearray(struct.pack("f", accx))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", accy))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", accz))
    data.extend(reversed(ba))
    (gyrox, gyroy, gyroz) = self.getgyro()
    ba = bytearray(struct.pack("f", gyrox))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", gyroy))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", gyroz))
    data.extend(reversed(ba))
    (magx, magy, magz) = self.getmag()
    ba = bytearray(struct.pack("f", magx))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", magy))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", magz))
    data.extend(reversed(ba))

    return data 

  def create_config_packet(self):
    # Configuration data
    # preamble
    if (self._cosmos_protocol == "tcp"):
      data = bytearray((0xDE, 0xAD, 0x4E, 0xAD))
    else:
      data = bytearray()
    # CCSDS primary header
    data.extend((0x0C, 0x03))
    # sequence
    data.extend((0xC0 | ((self._cfg_sequence >> 8) & 0xFF), self._cfg_sequence & 0xFF))
    self._cfg_sequence = self._cfg_sequence + 1
    if (self._cfg_sequence >=16384):
        self._cfg_sequence = 0
    # length - 1
    data.extend((0x01,0x26))
    # CCSDS secondary header; 4 byte time
    # CCSDS secondary header, ASCII segmented time code
    now = datetime.datetime.now()
    data.extend("%4.4d-%2.2d-%2.2dT%2.2d:%2.2d:%2.2d.%2.2d" % (now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond/10000))

    # Configuration data
    ba = bytearray(struct.pack("I", self._cos_cmd_recv))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("I", self._cos_cmd_succ))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("I", self._datalines))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("I", self._number_of_pics))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("I", len(os.listdir(self._basedirectory))))
    data.extend(reversed(ba))
    space = os.statvfs("/")
    ba = bytearray(struct.pack("f", (space.f_frsize * space.f_blocks) / (1024.0*1024.0))) # MB
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", (space.f_frsize * space.f_bfree) / (1024.0*1024.0))) # MB
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", (space.f_frsize * space.f_bavail) / (1024.0*1024.0))) # MB
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._acc_x_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._acc_x_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._acc_y_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._acc_y_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._acc_z_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._acc_z_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._gyro_x_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._gyro_x_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._gyro_y_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._gyro_y_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._gyro_z_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._gyro_z_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._mag_x_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._mag_x_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._mag_y_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._mag_y_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._mag_z_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._mag_z_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._temp_c_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._temp_c_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._temp_f_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._temp_f_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._press_mb_offset))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._press_mb_scale))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("I", self._cosmos_tcp_cmd_port))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("I", self._cosmos_tcp_tlm_port))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._cosmos_envtlm_delay))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._cosmos_imutlm_delay))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._cosmos_configtlm_delay))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._cosmos_idtlm_delay))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._loop_rate))
    data.extend(reversed(ba))
    ba = bytearray(struct.pack("f", self._sensor_loop_delay))
    data.extend(reversed(ba))
    flags = 0
    if (self._not_done):
      flags = flags + 128
    if (self._shut_down):
      flags = flags +  64
    if (self._reboot):
      flags = flags +  32
    if (self._debug):
      flags = flags +  16
    if (self._loop_debug):
      flags = flags +   8
    ba = bytearray(struct.pack("i", flags)[0])
    data.extend(ba)
    data.extend("%-48.48s" % self._directory)
    data.extend("%-64.64s" % self._datafilename)

    return data

  def create_id_packet(self):
    # time, hostname
    # preamble
    if (self._cosmos_protocol == "tcp"):
      data = bytearray((0xDE, 0xAD, 0x4E, 0xAD))
    else:
      data = bytearray()
    # CCSDS primary header
    data.extend((0x0C, 0x00))
    # sequence
    data.extend((0xC0 | ((self._id_sequence >> 8) & 0xFF), self._id_sequence & 0xFF))
    self._id_sequence = self._id_sequence + 1
    if (self._id_sequence >=16384):
        self._id_sequence = 0
    # length - 1
    data.extend((0x00,0x55))
    # CCSDS secondary header, ASCII segmented time code
    now = datetime.datetime.now()
    data.extend("%4.4d-%2.2d-%2.2dT%2.2d:%2.2d:%2.2d.%2.2d" % (now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond/10000))
    # ID string
    data.extend("%-64.64s" % self.getname())

    return data

  # Main git-r-done method
  def run(self):
    print("Send COSMOS commands to TCP port %d" % self._cosmos_tcp_cmd_port)
    print("Receive COSMOS telemetry from TCP port %d" % self._cosmos_tcp_tlm_port)

    last = datetime.datetime.now()
    while self._not_done:
      now = datetime.datetime.now()
      if (now > last + datetime.timedelta(0, self._sensor_loop_delay)):
        last = now
        self.loop()
      time.sleep(self._loop_rate)

    if (self._datafile is not None):
      self._datafile.close()
    print("Joining COSMOS cmd thread")
    self._cosmos_cmd_thread.join()
    print("Joining COSMOS tlm thread")
    self._cosmos_tlm_thread.join()
    print("Joining picture thread")
    self._picture_thread.join()

    print("Ending run method")
    if (self._camera is not None):
      self._camera.close()

    if (self._reboot):
      print("Rebooting")
      os.system("reboot now")
    if (self._shut_down):
      print("Shutting down")
      os.system("shutdown now")


  def get_offset(self, c, section, parm):
    try:
      v = c.getfloat(section, parm)
      if (v >= -100.0) and (v <= 100.0):
        return v
      else:
        return 0.0
    except Exception as e:
      print(e)
      return 0.0

  def get_scale(self, c, section, parm):
    try:
      v = c.getfloat(section, parm)
      if (v > 0.0) and (v <= 100.0):
        return v
      else:
        return 1.0
    except Exception as e:
      print(e)
      return 1.0

  def read_config(self):
    try:
      c = ConfigParser.RawConfigParser()
      c.read(self._config_filename)
      self._acc_x_offset = self.get_offset(c, 'Accelerometer', 'acc_x_offset')
      self._acc_x_scale  = self.get_scale (c, 'Accelerometer', 'acc_x_scale')
      self._acc_y_offset = self.get_offset(c, 'Accelerometer', 'acc_y_offset')
      self._acc_y_scale  = self.get_scale (c, 'Accelerometer', 'acc_y_scale')
      self._acc_z_offset = self.get_offset(c, 'Accelerometer', 'acc_z_offset')
      self._acc_z_scale  = self.get_scale (c, 'Accelerometer', 'acc_z_scale')
      self._gyro_x_offset = self.get_offset(c, 'Gyroscope', 'gyro_x_offset')
      self._gyro_x_scale  = self.get_scale (c, 'Gyroscope', 'gyro_x_scale')
      self._gyro_y_offset = self.get_offset(c, 'Gyroscope', 'gyro_y_offset')
      self._gyro_y_scale  = self.get_scale (c, 'Gyroscope', 'gyro_y_scale')
      self._gyro_z_offset = self.get_offset(c, 'Gyroscope', 'gyro_z_offset')
      self._gyro_z_scale  = self.get_scale (c, 'Gyroscope', 'gyro_z_scale')
      self._mag_x_offset = self.get_offset(c, 'Magnetometer', 'mag_x_offset')
      self._mag_x_scale  = self.get_scale (c, 'Magnetometer', 'mag_x_scale')
      self._mag_y_offset = self.get_offset(c, 'Magnetometer', 'mag_y_offset')
      self._mag_y_scale  = self.get_scale (c, 'Magnetometer', 'mag_y_scale')
      self._mag_z_offset = self.get_offset(c, 'Magnetometer', 'mag_z_offset')
      self._mag_z_scale  = self.get_scale (c, 'Magnetometer', 'mag_z_scale')
      self._temp_c_offset = self.get_offset(c, 'Environment', 'temp_c_offset')
      self._temp_c_scale  = self.get_scale (c, 'Environment', 'temp_c_scale')
      self._temp_f_offset = self.get_offset(c, 'Environment', 'temp_f_offset')
      self._temp_f_scale  = self.get_scale (c, 'Environment', 'temp_f_scale')
      self._press_mb_offset = self.get_offset(c, 'Environment', 'press_mb_offset')
      self._press_mb_scale  = self.get_scale (c, 'Environment', 'press_mb_scale')
    except Exception as e:
      print(e)

  def write_config(self):
    c = ConfigParser.RawConfigParser()
    c.add_section('Accelerometer')
    c.set('Accelerometer', 'acc_x_offset', str(self._acc_x_offset))
    c.set('Accelerometer', 'acc_x_scale', str(self._acc_x_scale))
    c.set('Accelerometer', 'acc_y_offset', str(self._acc_y_offset))
    c.set('Accelerometer', 'acc_y_scale', str(self._acc_y_scale))
    c.set('Accelerometer', 'acc_z_offset', str(self._acc_z_offset))
    c.set('Accelerometer', 'acc_z_scale', str(self._acc_z_scale))
    c.add_section('Gyroscope')
    c.set('Gyroscope', 'gyro_x_offset', str(self._gyro_x_offset))
    c.set('Gyroscope', 'gyro_x_scale', str(self._gyro_x_scale))
    c.set('Gyroscope', 'gyro_y_offset', str(self._gyro_y_offset))
    c.set('Gyroscope', 'gyro_y_scale', str(self._gyro_y_scale))
    c.set('Gyroscope', 'gyro_z_offset', str(self._gyro_z_offset))
    c.set('Gyroscope', 'gyro_z_scale', str(self._gyro_z_scale))
    c.add_section('Magnetometer')
    c.set('Magnetometer', 'mag_x_offset', str(self._mag_x_offset))
    c.set('Magnetometer', 'mag_x_scale', str(self._mag_x_scale))
    c.set('Magnetometer', 'mag_y_offset', str(self._mag_y_offset))
    c.set('Magnetometer', 'mag_y_scale', str(self._mag_y_scale))
    c.set('Magnetometer', 'mag_z_offset', str(self._mag_z_offset))
    c.set('Magnetometer', 'mag_z_scale', str(self._mag_z_scale))
    c.add_section('Environment')
    c.set('Environment', 'temp_c_offset', str(self._temp_c_offset))
    c.set('Environment', 'temp_c_scale', str(self._temp_c_scale))
    c.set('Environment', 'temp_f_offset', str(self._temp_f_offset))
    c.set('Environment', 'temp_f_scale', str(self._temp_f_scale))
    c.set('Environment', 'press_mb_offset', str(self._press_mb_offset))
    c.set('Environment', 'press_mb_scale', str(self._press_mb_scale))

    with open(self._config_filename, 'wb') as cfgfile:
      c.write(cfgfile)

  # Member data
  _bmp = None
  _imuok = False
  _camera = None
  _badvalues = (-99999, -99999, -99999)
  _any_ip = '0.0.0.0'
  _my_ip = '127.0.0.1'
  _cosmos_ip = {} 
  _cosmos_protocol = "udp" # "udp" or "tcp"
  _cosmos_tcp_cmd_port = 7779
  _cosmos_tcp_tlm_port = 7780
  _cosmos_envtlm_delay = 0.1
  _cosmos_imutlm_delay = 0.02
  _cosmos_configtlm_delay = 1.0
  _cosmos_idtlm_delay = 1.0
  _bme = None
  _loop_rate = 0.02
  _basedirectory = "/home/pi/%s-data" % os.uname()[1] 
  _directory = _basedirectory 
  _datafilename = ""
  _datafile = None
  _id_sequence = 0
  _env_sequence = 0
  _imu_sequence = 0
  _cfg_sequence = 0
  _take_picture = False
  _number_of_pics = 0
  _datalines = 0
  _cos_cmd_recv = 0
  _cos_cmd_succ = 0
  _config_filename = '/home/pi/sras/PiDataCollectionScripts/config.txt'

  # These are commandable
  _sensor_loop_delay = 0.02
  _not_done = True
  _shut_down = False
  _reboot = False
  _debug = False 
  _loop_debug = False

  _acc_x_offset = 0.0
  _acc_x_scale  = 1.0
  _acc_y_offset = 0.0
  _acc_y_scale  = 1.0
  _acc_z_offset = 0.0
  _acc_z_scale  = 1.0
  _gyro_x_offset = 0.0
  _gyro_x_scale  = 1.0
  _gyro_y_offset = 0.0
  _gyro_y_scale  = 1.0
  _gyro_z_offset = 0.0
  _gyro_z_scale  = 1.0
  _mag_x_offset = 0.0
  _mag_x_scale  = 1.0
  _mag_y_offset = 0.0
  _mag_y_scale  = 1.0
  _mag_z_offset = 0.0
  _mag_z_scale  = 1.0
  _temp_c_offset = 0.0
  _temp_c_scale  = 1.0
  _temp_f_offset = 0.0
  _temp_f_scale  = 1.0
  _press_mb_offset = 0.0
  _press_mb_scale  = 1.0

# Ok, really the main git-r-done method
def main():
  s = SensorCollector()
  s.run()

# Idiom to run main if this script is directly called
if __name__ == "__main__":
  main()

