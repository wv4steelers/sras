TELEMETRY SRAS IMU BIG_ENDIAN "SRAS Inertial Measurement Unit Data Packet"
APPEND_ITEM    PKT_VER   3 UINT   "CCSDS Protocol Version"
APPEND_ITEM    PKT_TYP   1 UINT   "CCSDS Packet Type (0 = TLM, 1 = CMD)"
APPEND_ITEM    SEC_HDR   1 UINT   "CCSDS Secondary Header Flag (1 = present, 0 = not present)"
APPEND_ID_ITEM APID     11 UINT   0x402 "CCSDS Telemetry ID Number"
APPEND_ITEM    SEQ_FLG   2 UINT   "CCSDS Sequence Flag (3d = 11b = not fragmented)"
APPEND_ITEM    SEQ_NUM  14 UINT   "CCSDS Sequence Number"
APPEND_ITEM    LENGTH   16 UINT   "CCSDS Data Length - 1"
APPEND_ITEM    DATETM  176 STRING "CCSDS Sec Hdr - ASCII segmented time code"
APPEND_ITEM    ACCX     32 FLOAT  "Acceleration X (camera), in g's"
APPEND_ITEM    ACCY     32 FLOAT  "Acceleration Y (right), in g's"
APPEND_ITEM    ACCZ     32 FLOAT  "Acceleration Z (up), in g's"
APPEND_ITEM    GYROX    32 FLOAT  "Angular rate (gyro) X (camera), in degrees/second"
APPEND_ITEM    GYROY    32 FLOAT  "Angular rate (gyro) Y (right), in degrees/second"
APPEND_ITEM    GYROZ    32 FLOAT  "Angular rate (gyro) Z (up), in degrees/second"
APPEND_ITEM    MAGX     32 FLOAT  "Magnetic field X (camera), in Gauss"
APPEND_ITEM    MAGY     32 FLOAT  "Magnetic field Y (right), in Gauss"
APPEND_ITEM    MAGZ     32 FLOAT  "Magnetic field Z (up), in Gauss"
ITEM           ACC      0 0 DERIVED "Magnitude of the acceleration, in g's"
  GENERIC_READ_CONVERSION_START FLOAT 32
  (Math.sqrt(packet.read("ACCX")*packet.read("ACCX") + packet.read("ACCY")*packet.read("ACCY") + packet.read("ACCZ")*packet.read("ACCZ")))
  GENERIC_READ_CONVERSION_END
ITEM           GYRO      0 0 DERIVED "Magnitude of the angular rate, in degrees/second"
  GENERIC_READ_CONVERSION_START FLOAT 32
  (Math.sqrt(packet.read("GYROX")*packet.read("GYROX") + packet.read("GYROY")*packet.read("GYROY") + packet.read("GYROZ")*packet.read("GYROZ")))
  GENERIC_READ_CONVERSION_END
ITEM           MAG       0 0 DERIVED "Magnitude of the magnetic field, in Gauss"
  GENERIC_READ_CONVERSION_START FLOAT 32
  (Math.sqrt(packet.read("MAGX")*packet.read("MAGX") + packet.read("MAGY")*packet.read("MAGY") + packet.read("MAGZ")*packet.read("MAGZ")))
  GENERIC_READ_CONVERSION_END

