clear
%%%% Read Input %%%%%
[file,folder]=uigetfile('*.csv','Select Rocket Telemetry File');
fileName=fullfile(folder,file);

fid = fopen(fileName);

f = fopen(fileName, 'rt');
ctr = 0;
ll = fgetl(f);
while (!isnumeric(ll)) 
    ctr = ctr+1;
    ll = fgetl(f);
end
fclose(f);
nRows = ctr;

%nRows = numel(textread(fileName,'%1c%*[^\n]'));



h = waitbar(0,'Loading Time Series...');
for l=1:(nRows-2)
    lineText = fgetl(fid); 
    lineSplit = strsplit(lineText,',');
    if l==1
        startTime=lineSplit(2);
    end
    
    t= strtrunc(lineSplit(2), 23);
    try
      time(l)=datenum(datevec(t,'yyyy-mm-dd HH:MM:SS.FFF'));	
     catch
      time(l)=datenum(datevec(t,'yyyy-mm-dd HH:MM:SS'));
    end
    waitbar(l/ nRows)
end
close(h)

elapsed=(time-time(1))*86400;

dt=diff(elapsed);

telemData= csvread(fileName,0,2);
tempC=telemData(1:(end-2),1);
tempF=telemData(1:(end-2),2); 
press=telemData(1:(end-2),3); %mBar
accel=telemData(1:(end-2),4:6)*9.80665; %m/s^2
gyro=telemData(1:(end-2),7:9)*pi/180; %rad/s
magnetometer=telemData(1:(end-2),10:12); %Gauss

%select time limits
figure()
plot(elapsed,vecnorm(accel'))
amax=max(vecnorm(accel'));
title('Select Start Time')
xlabel('Time (s)')
ylabel('Acceleration Magnitude')
hold on
[x(1),y(1)]=ginput(1);

line([x(1) x(1)], [0 amax])
title('Select End Time')
[x(2),y(2)]=ginput(1);
line([x(2) x(2)], [0 amax])

y=[0;0];


belowRange = (elapsed < x(1));
aboveRange = (elapsed > x(2));

Remove = or(belowRange,aboveRange);
aRange=vecnorm(accel');
aRange(Remove) = NaN;
xRange=elapsed;
xRange(Remove) = NaN;

plot(xRange,aRange)
elapsed2=elapsed(~isnan(xRange));
elapsed2=elapsed2-elapsed2(1);
elapsed=elapsed2;


dt=diff(elapsed2);

tempC=tempC(~isnan(xRange));
tempF=tempF(~isnan(xRange));
press=press(~isnan(xRange)); %mBar
accel=accel((~isnan(xRange)),:); %m/s^2
gyro=gyro((~isnan(xRange)),:); %rad/s
magnetometer=magnetometer((~isnan(xRange)),:);; %Gauss



numEpochs=length(gyro);
euler=zeros(numEpochs,3);
pos=zeros(numEpochs,3);

dv_timeseries=zeros(numEpochs,3);
h = waitbar(0,'Calculating Trajectory...');
%gVector=[0 0 -9.80665]';
gVector=accel(1,:)';
%[ asin(gVector(1)/norm(gVector)) asin(gVector(2)/norm(gVector)) asin(gVector(3)/norm(gVector))];
euler(1,:)=[0 0 0];
gVectorTruth=[0 0 9.80665]'
accelBias=accel(1,:)'-gVectorTruth;



for t=2:(numEpochs-1)
    psi=euler(t,1);
    theta=euler(1,2);
    phi=euler(1,3);
    
    euler(t,:)=[0, sin(phi)*sec(theta), cos(phi)*sec(theta);...
        0, cos(phi), -1*sin(theta);...
        1, sin(phi)*tan(theta), cos(phi)*tan(theta)]*gyro(t,:)'*dt(t)+euler(t-1,:);
    
    dv_inertial=([cos(psi) -sin(psi) 0; sin(psi) cos(psi) 0; 0 0 1]*[cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)]*[1 0 0; 0 cos(phi) -sin(phi); 0 sin(phi) cos(phi)]*(accel(t,:)'-accelBias)-gVectorTruth)*dt(t);
    dv_timeseries(t,:)=dv_inertial';
    pos(t,:)=pos(t-1,:)+dv_inertial'*dt(t);
    waitbar(t/ numEpochs)

end
close(h)




save flightData

%%% Plot Output %%%

figure()
subplot(3,1,1)
plot(elapsed,pos(:,1))
title('Position')
xlabel('t (s)')
ylabel('X (m)')
subplot(3,1,2)
plot(elapsed,pos(:,2))
xlabel('t (s)')
ylabel('Y (m)')
subplot(3,1,3)
plot(elapsed,pos(:,3))
xlabel('t (s)')
ylabel('Z (m)')

figure()
subplot(3,1,1)
plot(elapsed,euler(:,1)*180/pi)
title('Attitude')
xlabel('t (s)')
ylabel('Roll (degrees)')
subplot(3,1,2)
plot(elapsed,euler(:,2)*180/pi)
xlabel('t (s)')
ylabel('Pitch (degrees)')
subplot(3,1,3)
plot(elapsed,euler(:,3)*180/pi)
xlabel('t (s)')
ylabel('Yaw (degrees)')


figure()
subplot(2,1,1)
plot(elapsed,magnetometer)
xlabel('t (s)')
title('Magnetic Field X Y Z(Gauss)')
subplot(2,1,2)
scatter(pos(:,3),vecnorm(magnetometer'))
xlabel('Height (m)')
title('Magnetic Field Strength(Gauss)')


figure()
subplot(2,1,1)
plot(elapsed,tempC)
xlabel('t (s)')
title('Temperature (C)')
subplot(2,1,2)
scatter(pos(:,3),tempC)
xlabel('Height (m)')
title('Temperature (C)')


figure()
subplot(2,1,1)
plot(elapsed,press)
xlabel('t (s)')
title('Pressure (mBar)')
subplot(2,1,2)
scatter(pos(:,3),press)
xlabel('Height (m)')
title('Pressure (mBar)')


figure()
subplot(3,1,1)
plot(elapsed,accel(:,1))
title('Accelerometer Measurements')
xlabel('t (s)')
ylabel('X (m/s^2)')
subplot(3,1,2)
plot(elapsed,accel(:,2))
xlabel('t (s)')
ylabel('Y (m/s^2)')
subplot(3,1,3)
plot(elapsed,accel(:,3))
xlabel('t (s)')
ylabel('Z (m/s^2)')

figure()
subplot(3,1,1)
plot(elapsed,gyro(:,1)*180/pi)
title('Gyro Measurements')
xlabel('t (s)')
ylabel('Roll (deg/sec)')
subplot(3,1,2)
plot(elapsed,gyro(:,2)*180/pi)
xlabel('t (s)')
ylabel('Pitch (deg/sec)')
subplot(3,1,3)
plot(elapsed,gyro(:,3)*180/pi)
xlabel('t (s)')
ylabel('Yaw (deg/sec)')


%%% Trajectory Animation %%%

btn = questdlg ('Animate Rocket Trajectory?')
if btn(1)=='Y'
  %timeScale=1/100;
  scale=max(max(pos));
  figure
  axis([-1 1 -1 1 0 1]*scale)
  hold on
  x=pos(:,1);
  y=pos(:,2);
  z=pos(:,3);
  for ii=1:(length(pos)-1)
      plot3 (z(ii), y(ii), z(ii),'*');
      %pause (dt(ii)*timeScale)
      pause(0.0001)
      Tlabel=strcat('T = ',mat2str(elapsed(ii)));
      xlabel(Tlabel)
  end
end



