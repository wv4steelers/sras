![Picture of SRAS Payload](./SRAS.jpg "SRAS Payload")

# README #

Sounding Rockets for All Students (SRAS) is the latest payload for middle/high school summer camps designed by the NASA IV&V ERC in coordination with the West Virginia Rocketry Association.
This camp was first run during the summer of 2018 and builds on things learned during previous rocketry summer camps, including ones based on the S4 curriculum developed by Sonoma College (http://s4.sonoma.edu/).

The SRAS payload is designed to fit in place of the egg payload in a rocket built for the Team America Rocketry Challenge (TARC, http://rocketcontest.org/).  The payload fits in a standard BT-80 (65 mm inside diameter) model rocketry tube, is approximately 165 mm long, and weighs approximately 175g.

The SRAS payload features a 3D printed sled; a Raspberry Pi Zero W computer; an inertial measurement unit consisting of accelerometers, gyroscopes, magnetometers, thermometers, and air pressure gauges; a camera; and a battery pack.

The goal is for students to solder some components, assemble the sled and components, perform some programming of the Pi for gathering data during rocket flight, and retrieve and analyze the data from the payload following flight.

### What is this repository for? ###

This repository contains:

- Instructions (README.md and documentation folder)
- The payload sled 3D design files (3D-sled-parts)
- The PiBakery configuration for creating the SD card for the Pi (PiBakery)
- Python scripts to run on the Pi to capture, store, and broadcast data from the IMU and camera (PiDataCollectionScripts)
- Configuration files for setting up monitoring using the COSMOS ground station software (COSMOS-config)
- And example files from running the SRAS software on the payload (example-data)

### Summary Instructions ###
Build:

- Build a TARC rocket, using a BT-80 body tube for the payload bay and reserving enough space and mass for the payload.
- 3-D print the sled
- Solder the wires from the Pi Zero W to the IMU
- Solder the LED/resistor and the 2 header pins to the Pi Zero W
- Assemble the sled, the Pi Zero W, the IMU, the Camera, and the Battery
- Using a computer and PiBakery, burn the SD card for the Pi Zero W
- Insert the SD card in the Pi Zero W and start it up
- Using a computer and FileZilla (for transferring files from your computer to the Pi) and Notepad++ (or another text editor), program the SD card in the Pi Zero W for saving IMU data to a file and for taking pictures with the camera (i.e. make edits to the file /home/pi/sras/PiDataCollectionScripts/SRAS.py)

Operate:

- (Optional) Using a computer, onfigure the COSMOS ground station software to connect wirelessly to the Pi Zero W to command the payload and receive telemetry data from the payload
- Power on the SRAS payload, launch and retrieve the TARC rocket with the SRAS payload, and power down the SRAS payload.
- Using a computer and LinuxReader, read the IMU data and the camera pictures from the Pi Zero SD card (after pulling it out of the SRAS payload).

### Parts Lists, Detailed Development Instructions, Operating Instructions, and Additional Pictures ###
- [Parts Lists](./documentation/PartsList.md)
- [Detailed Development Instructions](./documentation/DetailedInstructions.md)
- [Operating Instructions](./documentation/OperatingInstructions.md)
- [Additional Pictures](./documentation/Pictures.md) (see .jpg and .png files in the documentation directory)

### Who do I talk to? ###

* https://www.nasa.gov/centers/ivv/education/educators.html
* https://www.facebook.com/wvrocketry
* http://www.wvrocketry.org/
* WVROCKETRY-L@LISTSERV.WVNET.EDU
