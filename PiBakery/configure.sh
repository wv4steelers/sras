#!/bin/bash

# Need to enable camera for Pi
if [ ! -e /root/enablecamera.ok ]; then
  sed /boot/config.txt -i -e "s/^startx/#startx/" > /root/enablecamera.log 2>&1 # Comment out old startx setting... if there is one
  sed /boot/config.txt -i -e "s/^gpu_mem/#gpu_mem/" >> /root/enablecamera.log 2>&1  # Comment out old gpu_mem setting... if there is one
  (echo "start_x=1" >> /boot/config.txt) >> /root/enablecamera.log 2>&1             # essential
  (echo "gpu_mem=128" >> /boot/config.txt) >> /root/enablecamera.log 2>&1           # at least, or maybe more if you wish
  # echo "disable_camera_led=1" >> /boot/config.txt >> /root/enablecamera.log 2>&1  # optional, if you don't want the led to glow
  touch /root/enablecamera.ok
fi
	
# Retrieve the SRAS code
if [ ! -e /home/pi/sras ]; then
  cd /home/pi > /root/getsras.log 2>&1
  git clone https://wv4steelers@bitbucket.org/wv4steelers/sras.git >> /root/getsras.log 2>&1
  chown -R pi:pi sras >> /root/getsras.log 2>&1
fi

# Finally, set script to run at boot
grep SRAS /etc/rc.local > /dev/null
if [ $? -eq 1 ]; then
  sed -i -e 's#^exit 0#/home/pi/sras/PiDataCollectionScripts/SRAS.py \&\n\nexit 0#;' /etc/rc.local > /root/rclocal.log 2>&1
  reboot now
fi

