#!/usr/bin/env python

import os
import time
import threading
import socket
import RPi.GPIO as GPIO

_status_pin = 23
_shutdown_pin = 12

def main():
  GPIO.setwarnings(False)
  GPIO.setmode(GPIO.BCM)
  GPIO.setup(_status_pin, GPIO.OUT)
  GPIO.setup(_shutdown_pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)

  shutdown_pin_thread = threading.Thread(target=shutdown_pin_handler, args=())
  shutdown_pin_thread.start()

  while True:
    try:
      # Long blink to start sequence
      GPIO.output(_status_pin, 1)
      time.sleep(3)

      ret = os.system("ps -ef | grep SRAS.py | egrep -v grep")
      print(ret)
      if (ret == 0):
        # Another long blink if SRAS.py process is found
        GPIO.output(_status_pin, 0)
        time.sleep(1)
        GPIO.output(_status_pin, 1)
        time.sleep(3)
      GPIO.output(_status_pin, 0)
      time.sleep(1)
     
      # Now blink out the IP address
      ip = ((([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0])
      print(ip)
      for c in ip:
        print(c)
        blink(c)
    except:
      # Something bad happened... wait and try again.
      GPIO.output(_status_pin, 0)
      time.sleep(3)

# Function executed in a separate thread to monitor the shutdown I/O pin
def shutdown_pin_handler():
  while True:
    if not GPIO.input(_shutdown_pin):
      os.system("shutdown now")
    time.sleep(1) 

def blink(c):
  if (c == "."):
    # Pause for the .
    time.sleep(1)
    return

  try:
    num = int(c)

    if (num == 0):
      # Blink 10 times for 0
      num = 10

    # Blink number of times for num
    for i in range(0,num):
      GPIO.output(_status_pin, 0)
      time.sleep(0.2)
      GPIO.output(_status_pin, 1)
      time.sleep(0.2)
  except:
    # Was not a . or 0-9 ... so just blink an error blink
    GPIO.output(_status_pin, 0)
    time.sleep(0.5)
    GPIO.output(_status_pin, 1)
    time.sleep(0.5)

  # Pause after output
  GPIO.output(_status_pin, 0)
  time.sleep(0.5)

# Idiom to run main if this script is directly called
if __name__ == "__main__":
  main()

