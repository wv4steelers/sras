#!/bin/bash

# Simple boot counter
if [ ! -e /root/numberofboots ]; then
  echo 0 > /root/numberofboots
else
  x=$(expr $(cat /root/numberofboots) + 1)
  echo $x > /root/numberofboots
fi

# Set LED IP/status blinker script to run at boot
grep led.py /etc/rc.local > /dev/null
if [ $? -eq 1 ]; then
  sed -i -e 's#^exit 0#/root/led.py \&\n\nexit 0#;' /etc/rc.local > /root/rclocal.log 2>&1
  /root/led.py &
fi

